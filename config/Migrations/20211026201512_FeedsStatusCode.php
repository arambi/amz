<?php
use Migrations\AbstractMigration;

class FeedsStatusCode extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('amz_feeds')
            ->addColumn('result_proccess', 'boolean', ['null' => false, 'default' => 0])
            ->addColumn('result_success', 'boolean', ['null' => false, 'default' => 0])
            ->changeColumn('error_message', 'text', ['null' => true, 'default' => null])
            ->addIndex('result_proccess')
            ->update();
    }
}
