<?php

use Migrations\AbstractMigration;

class Feeds extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('amz_feeds')
            ->addColumn('sku', 'string', ['limit' => 32, 'null' => false])
            ->addColumn('feed_id', 'string', ['limit' => 32, 'default' => null, 'null' => true])
            ->addColumn('feed_type', 'string', ['limit' => 32, 'null' => false])
            ->addColumn('result_feed_document_id', 'string', ['limit' => 80, 'default' => null, 'null' => true])
            ->addColumn('error_message', 'string', ['null' => true, 'default' => null])
            ->addColumn('processing_status', 'string', ['limit' => 36, 'null' => true, 'default' => null])
            ->addColumn('xml', 'text', ['null' => false])
            ->addColumn('locked', 'boolean', ['null' => false, 'default' => 0])
            ->addColumn('errors', 'integer', ['null' => false, 'default' => 0, 'limit' => 2])
            ->addColumn('created', 'datetime', array('default' => null))
            ->addColumn('modified', 'datetime', array('default' => null))
            ->addIndex('feed_id')
            ->addIndex('errors')
            ->addIndex('locked')
            ->create();
    }
}