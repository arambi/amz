<?php

namespace Amz\Interfaces;


interface ProductAmzInterface
{
    public function getAmzTitle(): string;

    public function getAmzDescription(): string;

    public function getAmzSku(): string;

    public function getAmzPrice(): float;

    public function getAmzEAN(): string;

    public function getAmzBrand(): string;

    public function getAmzMfrPartNumber(): string;

    public function getAmzImages(): array;

    public function getAmzBulletPoints(\SimpleXMLElement $descriptionElement);

    public function isAmzValid(): bool;

    public function getAmzQuantity(): int;
}
