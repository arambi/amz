<?php

namespace Amz\Interfaces;

use Cake\ORM\Query;


interface ProductsTableAmzInterface
{
    public function findAmazon(Query $query, array $options): Query;
}
