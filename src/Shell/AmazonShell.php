<?php

namespace Amz\Shell;

use Amz\Feed\Image;
use Amz\Feed\Price;
use Amz\Feed\Product;
use Amz\Upload\Upload;
use Amz\Feed\Inventory;
use Cake\Console\Shell;
use Amz\Report\FeedInfo;
use Cake\Core\Configure;
use Store\Config\StoreConfig;
use ClouSale\AmazonSellingPartnerAPI\Models\Feeds\Feed;

/**
 * Amazon shell command.
 */
class AmazonShell extends Shell
{

    private $config;
    
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $this->out($this->OptionParser->help());
    }

    private function getProductModel()
    {
        return StoreConfig::getProductsModel();
    }

    private function createClasses()
    {
        $this->FeedProduct = new Product($this->config);
        $this->FeedInventory = new Inventory($this->config);
        $this->FeedPrice = new Price($this->config);
        $this->FeedImage = new Image($this->config);
    }

    private function saveXmls()
    {
        $this->FeedProduct->saveXml();
        $this->FeedInventory->saveXml();
        $this->FeedPrice->saveXml();
        $this->FeedImage->saveXml();
    }

    public function update()
    {
        if (!isset($this->args[0])) {
            $this->abort('Es necesario indicar el nombre de la configuración');
        }

        $this->config = $this->args[0];
        $max = isset($this->args[1]) ? $this->args[1] : false;
        $config = Configure::read($this->config);
        $this->createClasses();
        $limit = 1;
        $offset = 0;

        while (true) {
            $query = $this->getProductModel()->find($config['finder'])
                ->order([
                    $this->getProductModel()->getAlias() . '.id' => 'desc'
                ])
                ->offset($offset)
                ->limit($limit);

            $products = $query->all();
            $products = $products->filter(function ($product) {
                return $product->isAmzValid();
            });

            if ($products->count() == 0 || ($max && $offset > $max)) {
                $this->out('FIN');
                break;
            }

            /**
             * @var \Amz\Interfaces\ProductAmzInterface $product
             */
            $this->_doProduct($products->toArray());
            $offset = $offset + $limit;
            
            break;
        }

        $this->saveXmls();
    }

    public function updateOne()
    {
        $id = $this->in('Indica el ID del producto');

        $product = $this->getProductModel()->find('amazon')
            ->where([
                $this->getProductModel()->getAlias() . '.id' => $id
            ])
            ->first();

        /**
         * @var \Amz\Interfaces\ProductAmzInterface $product
         */
        $this->_doProduct($product);
    }


    private function _doProduct($products)
    {
        $classes = [
            'FeedProduct',
            'FeedInventory',
            'FeedPrice',
            'FeedImage',
        ];

        foreach ($classes as $class) {
            foreach ($products as $product) {
                $this->out($class . ' - ' . $product->id);
                $this->$class->addMessage($product);
            }
        }
    }


    public function upload()
    {
        $this->config = $this->args[0];
        $feeds = $this->getTableLocator()->get('Amz.Feeds')
            ->find('feeds');

        /**
         * @var \Amz\Model\Entity\Feed $feed
         */
        foreach ($feeds as $key => $feed) {
            // $feed->setLocked();
            (new Upload($feed, $this->config))->upload();
            $feed->setUnlocked();

            if ($key % 4 == 0) {
                sleep(30);
            }
        }
    }

    public function statuses()
    {
        $feeds = $this->getTableLocator()->get('Amz.Feeds')
            ->find('feedsForStatus');

        foreach ($feeds as $feed) {
            $this->out("Procesando {$feed->feed_id}");
            try {
                $result = (new FeedInfo())->getFeed($feed->feed_id);
                $status = $result->getProcessingStatus();
                $set = [
                    'processing_status' => $status,
                ];

                if ($status == Feed::PROCESSING_STATUS_DONE) {
                    $set['result_feed_document_id'] = $result->getResultFeedDocumentId();
                }

                $this->out("Estado: {$status}");
                $this->getTableLocator()->get('Amz.Feeds')->query()->update()
                    ->set($set)
                    ->where([
                        'id' => $feed->id
                    ])
                    ->execute();
            } catch (\Throwable $th) {
                $this->out((string)$th->getMessage());
            }
        }
    }

    public function reports()
    {
        $feeds = $this->getTableLocator()->get('Amz.Feeds')
            ->find('feedsForReports');

        foreach ($feeds as $feed) {
            $this->out("Procesando {$feed->feed_id}");

            try {
                $result = (new FeedInfo())->getReport($feed->result_feed_document_id);

                if ($result['Message']['ProcessingReport']['ProcessingSummary']['MessagesWithError'] == '0') {
                    $set = [
                        'result_success' => true,
                        'result_proccess' => true,
                    ];
                } else {
                    $set = [
                        'result_success' => true,
                        'result_proccess' => true,
                        'error_message' => $result['Message']['ProcessingReport']['Result'],
                    ];
                }

                $this->getTableLocator()->get('Amz.Feeds')->query()->update()
                    ->set($set)
                    ->where([
                        'id' => $feed->id
                    ])
                    ->execute();
            } catch (\Throwable $th) {
                $this->out((string)$th->getMessage());
            }

            sleep(6);
        }
    }

    public function feed()
    {
        $feed_id = $this->in('Indica el Feed ID');
        $result = (new FeedInfo())->getFeed($feed_id);
        print_r($result);
    }

    public function feeds()
    {
        $types = [
            1 => 'POST_PRODUCT_DATA',
            2 => 'POST_INVENTORY_AVAILABILITY_DATA',
            3 => 'POST_PRODUCT_PRICING_DATA',
            4 => 'POST_PRODUCT_IMAGE_DATA',
        ];

        foreach ($types as $key => $type) {
            $this->out("$key. $type");
        }

        $key = $this->in('Indica el tipo de feed');
        $result = (new FeedInfo())->getFeeds($types[$key]);
        print_r($result);
    }

    public function cancelFeeds()
    {
        $types = [
            1 => 'POST_PRODUCT_DATA',
            2 => 'POST_INVENTORY_AVAILABILITY_DATA',
            3 => 'POST_PRODUCT_PRICING_DATA',
            4 => 'POST_PRODUCT_IMAGE_DATA',
        ];

        foreach ($types as $key => $type) {
            $result = (new FeedInfo())->cancelFeeds($type);
        }
    }

    public function cancelFeed()
    {
        $id = $this->in('Indica el id del feed');
        $result = (new FeedInfo())->cancelFeed($id);
        print_r($result);
    }


    public function report()
    {
        $doc_id = $this->in('Indica el document ID');
        $result = (new FeedInfo())->getReport($doc_id);
        print_r($result);
    }
}
