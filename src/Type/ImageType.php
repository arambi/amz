<?php

namespace Amz\Type;


class ImageType
{
    const MAIN = 'Main';
    const SWATCH = 'Swatch';
    const BKLB = 'BKLB';
    const PT1 = 'PT1';
    const PT2 = 'PT2';
    const PT3 = 'PT3';
    const PT4 = 'PT4';
    
}