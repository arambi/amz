<?php


namespace Amz\Report;

use Amz\Config\Config;
use ClouSale\AmazonSellingPartnerAPI\Api\FeedsApi;
use ClouSale\AmazonSellingPartnerAPI\ApiException;
use ClouSale\AmazonSellingPartnerAPI\Models\Feeds\Feed;

/**
 * @property \Amz\Config\Config $config
 */

class FeedInfo
{
    private $config;

    public function __construct()
    {
        $this->config = Config::instance();
    }

    public function getFeed($id)
    {
        $api = $this->getFeedsApi();

        try {
            $feed = $api->getFeed($id);
            return $feed->getPayload();
        } catch (ApiException $e) {
            $time = time();
            $message = $e->getMessage();
            $error = <<<JSON
                {
                    "error": "Error while getting feed data",
                    "body": $message,
                    "time": $time
                }
            JSON;
            echo $error;
        }
    }

    private function getFeedsApi()
    {
        $config = $this->config->createConfig();
        $api = new FeedsApi($config);
        return $api;
    }

    public function getFeeds($type)
    {
        $api = $this->getFeedsApi();

        try {
            $response = $api->getFeeds($type, null, 50, Feed::PROCESSING_STATUS_IN_PROGRESS);
            return $response->getPayload();
        } catch (ApiException $e) {
            $time = time();
            $message = $e->getMessage();
            $error = <<<JSON
                {
                    "error": "Error while getting feed data",
                    "body": $message,
                    "time": $time
                }
            JSON;
            echo $error;
        }
    }

    public function cancelFeeds($type)
    {
        $api = $this->getFeedsApi();

        try {
            $response = $api->getFeeds($type, null, 50, 'IN_QUEUE');
            $feeds = $response->getPayload();

            foreach ($feeds as $feed) {
                $res = $api->cancelFeed($feed->getFeedId());
                print((string)$res);
            }
        } catch (ApiException $e) {
        }
    }

    public function cancelFeed($id)
    {
        $api = $this->getFeedsApi();

        try {
            $res = $api->cancelFeed($id);
            print((string)$res);
        } catch (ApiException $e) {
            print_r($e->getMessage());
        }
    }

    public function getReport($docId)
    {
        $config = $this->config->createConfig();
        $api = new FeedsApi($config);
        $docResponse = $api->getFeedDocument($docId);
        $file = $api->downloadFeedProcessingReport($docResponse->getPayload());
        return $file;
    }
}
