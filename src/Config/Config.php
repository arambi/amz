<?php

namespace Amz\Config;

use Cake\Core\Configure;
use ClouSale\AmazonSellingPartnerAPI\AssumeRole;
use ClouSale\AmazonSellingPartnerAPI\Configuration;
use ClouSale\AmazonSellingPartnerAPI\SellingPartnerOAuth;

class Config
{
    public static $wrapper;
    public static $_instance;

    public static function instance($config)
    {
        if (empty(self::$_instance)) {
            self::$_instance = new Config($config);
        }

        return self::$_instance;
    }

    public function __construct($config)
    {
        self::$wrapper = Configure::read($config);
    }

    public function __set($name, $value)
    {
        self::$wrapper[$name] = $value;
    }

    public function __get($name)
    {
        return self::$wrapper[$name];
    }

    public function createConfig()
    {
        try {
            $accessToken = SellingPartnerOAuth::getAccessTokenFromRefreshToken(
                $this->refresh_token,
                $this->client_id,
                $this->client_secret
            );
            
            $assumeRole = AssumeRole::assume(
                $this->region,
                $this->access_key,
                $this->secret_key,
                $this->role_arn
            );

            $configuration = Configuration::getDefaultConfiguration();
            $configuration->setHost($this->endpoint);
            $configuration->setAccessToken($accessToken);
            $configuration->setAccessKey($assumeRole->getAccessKeyId());
            $configuration->setSecretKey($assumeRole->getSecretAccessKey());
            $configuration->setRegion($this->region);
            $configuration->setSecurityToken($assumeRole->getSessionToken());
            return $configuration;
        } catch (\Throwable $e) {
            // self::$log->info($e->getMessage());
        }

        return false;
    }

    public function toJson()
    {
        return json_encode(self::$wrapper, JSON_PRETTY_PRINT);
    }
}
