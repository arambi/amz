<?php
namespace Amz\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;
use Cake\Database\Schema\TableSchema;
use ClouSale\AmazonSellingPartnerAPI\Models\Feeds\Feed;

/**
 * Feeds Model
 *
 * @property \Amz\Model\Table\FeedsTable&\Cake\ORM\Association\BelongsTo $Feeds
 * @property \Amz\Model\Table\ResultFeedDocumentsTable&\Cake\ORM\Association\BelongsTo $ResultFeedDocuments
 *
 * @method \Amz\Model\Entity\Feed get($primaryKey, $options = [])
 * @method \Amz\Model\Entity\Feed newEntity($data = null, array $options = [])
 * @method \Amz\Model\Entity\Feed[] newEntities(array $data, array $options = [])
 * @method \Amz\Model\Entity\Feed|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Amz\Model\Entity\Feed saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Amz\Model\Entity\Feed patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Amz\Model\Entity\Feed[] patchEntities($entities, array $data, array $options = [])
 * @method \Amz\Model\Entity\Feed findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FeedsTable extends Table
{
    const MAX_ERRORS = 4;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('amz_feeds');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }


    public function findFeeds(Query $query, array $options)
    {
        return $query
            ->where([
                'Feeds.feed_id IS NULL',
                'Feeds.locked' => false,
                'Feeds.errors <=' => self::MAX_ERRORS  
            ])
            ->order([
                'Feeds.created' => 'asc'
            ]);
    }

    public function findFeedsForStatus(Query $query, array $options)
    {
        return $query
            ->where([
                'Feeds.feed_id IS NOT NULL',
                'OR' => [
                    'Feeds.processing_status NOT IN' => [
                        Feed::PROCESSING_STATUS_DONE,
                        Feed::PROCESSING_STATUS_FATAL,
                        Feed::PROCESSING_STATUS_CANCELLED,
                    ],
                    'Feeds.processing_status IS NULL',
                ],
                'Feeds.result_feed_document_id IS NULL',
            ])
            ->order([
                'Feeds.created' => 'asc'
            ]);
    }

    public function findFeedsForReports(Query $query, array $options)
    {
        return $query
            ->where([
                'Feeds.result_proccess' => false,
                'Feeds.result_feed_document_id IS NOT NULL',
            ])
            ->order([
                'Feeds.created' => 'asc'
            ]);
    }

    protected function _initializeSchema(TableSchema $schema)
    {
        $schema->setColumnType('error_message', 'json');
        return $schema;
    }
}
