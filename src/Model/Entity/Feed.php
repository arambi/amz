<?php
namespace Amz\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Feed Entity
 *
 * @property int $id
 * @property string $sku
 * @property string|null $feed_id
 * @property string $feed_type
 * @property string|null $result_feed_document_id
 * @property string|null $error_message
 * @property string|null $processing_status
 * @property string $xml
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \Amz\Model\Entity\Feed $feed
 * @property \Amz\Model\Entity\ResultFeedDocument $result_feed_document
 */
class Feed extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    private function updateField($key, $value)
    {
        TableRegistry::getTableLocator()->get($this->getSource())->query()->update()
            ->set([
                $key => $value, 
            ])
            ->where([
                'id' => $this->id
            ])
            ->execute();
    }

    public function setLocked()
    {
        $this->updateField('locked', true);
    }

    public function setUnlocked()
    {
        $this->updateField('locked', false);
    }

    public function addError()
    {
        $this->updateField('errors', $this->errors + 1);
    }
}
