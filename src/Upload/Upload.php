<?php

namespace Amz\Upload;

use Amz\Config\Config;
use Cake\ORM\Locator\LocatorAwareTrait;
use ClouSale\AmazonSellingPartnerAPI\AssumeRole;
use ClouSale\AmazonSellingPartnerAPI\Api\FeedsApi;
use ClouSale\AmazonSellingPartnerAPI\ApiException;
use ClouSale\AmazonSellingPartnerAPI\Configuration;
use ClouSale\AmazonSellingPartnerAPI\SellingPartnerOAuth;
use ClouSale\AmazonSellingPartnerAPI\Models\Feeds\CreateFeedSpecification;
use ClouSale\AmazonSellingPartnerAPI\Models\Feeds\CreateFeedDocumentSpecification;

/**
 * @property \Amz\Model\Entity\Feed $feed
 * @property \Amz\Config\Config $config
 * @property \Amz\Model\Table\FeedsTable $Feeds
 * 
 */

class Upload
{
    use LocatorAwareTrait;

    private $feed;
    
    private $config;

    private static $api;

    public function __construct($feed, $config)
    {
        $this->Feeds = $this->getTableLocator()->get('Amz.Feeds');
        $this->feed = $feed;
        $this->config = Config::instance($config);
        $this->file = TMP . $this->feed->xml;
    }


    protected static function getApi($config)
    {
        if (self::$api) {
            return self::$api;
        }
        
        $api = new FeedsApi($config);
        self::$api = $api;
        return $api;
    }

    public function upload()
    {
        $feedDoc = $this->createFDoc();
        $config = $this->config->createConfig();
        $api = self::getApi($config);
        try {
            $response = $api->uploadFeedDocument($feedDoc, 'text/xml; charset=utf-8', $this->file);

            if (strtolower($response) != 'done') {
                $time = time();
                $error = <<<JSON
                    {
                        "error": "document upload error (not done)",
                        "body": $response,
                        "time": $time
                    }
                JSON;
                echo $error;
                exit(1);
            }
            $feed = new CreateFeedSpecification([
                "feed_type" => $this->feed->feed_type,
                "marketplace_ids" => $this->config->marketplaces_ids,
                "input_feed_document_id" => $feedDoc->getFeedDocumentId()
            ]);

            sleep(1);
            $feedResponse =  $api->createFeed($feed);
            $payload = json_decode($feedResponse->getPayload());
            $this->updateEntity($payload->feedId);
        } catch (ApiException $e) {
            $message = $e->getMessage();
            $error = <<<JSON
                {
                    "error": $message,
                    "body": $response,
                }
            JSON;
            echo $error;
        }
    }

    private function updateEntity($feed_id)
    {
        $this->Feeds->query()->update()
            ->set([
                'feed_id' => $feed_id
            ])
            ->where([
                'id' => $this->feed->id
            ])
            ->execute();
    }

    private function createFDoc()
    {
        $config = $this->config->createConfig();
        if (!$config) {
            $time = time();
            $msg = <<<JSON
            {
                "error": "Error with configuration, enable verbose option to see error",
                "time": $time
            }
            JSON;
            echo $msg;
            exit(1);
        }
        $api = new FeedsApi($config);
        $requesBody = new CreateFeedDocumentSpecification([
            'content_type' => 'text/xml; charset=utf-8'
        ]); 
        try {
            $response = $api->createFeedDocument($requesBody);
            return $response->getPayload();
        } catch (ApiException $e) {
            $time = time();
            $msg = <<<JSON
            {
                "error": {$e->getMessage()},
                "time": $time
            }
            JSON;
            echo $msg;
            exit(1);
        }
        return false;
    }
}
