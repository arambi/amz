<?php

namespace Amz\Feed;

use Amz\Config\Config;
use Amz\Interfaces\ProductAmzInterface;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Utility\Text;

/**
 * @property \Amz\Model\Table\FeedsTable $Feeds
 */

abstract class Feed
{
    use LocatorAwareTrait;

    public static $name = '';

    /**
     * @var array $products
     */
    protected $products;

    protected $feedType;

    protected $replace = 'false';

    protected $operation = 'Update';

    protected $document;

    protected $root;

    /**
     * @var Config $config;
     */
    protected $config;

    public function __construct($config)
    {
        $this->config = Config::instance($config);

        if (is_callable($this->config->callback)) {
            $method = $this->config->callback;
            $method();
        }
        
        $this->Feeds = $this->getTableLocator()->get('Amz.Feeds');
        $this->createDocument();
    }

    private function createDocument()
    {
        $this->document = new \DOMDocument('1.0');
        $this->root = $this->document->createElement('AmazonEnvelope');
        $this->root->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        $this->root->setAttribute("xsi:noNamespaceSchemaLocation", "amzn-envelope.xsd");
        $this->root = $this->document->appendChild($this->root);
        $header = $this->document->createElement('Header');
        $header->appendChild($this->document->createElement("DocumentVersion", "1.01"));
        $header->appendChild($this->document->createElement("MerchantIdentifier", $this->config->vendor_id));
        $this->root->appendChild($header);
        $this->root->appendChild($this->document->createElement("MessageType", static::$name));
        $this->root->appendChild($this->document->createElement("PurgeAndReplace", $this->replace));
    }

    public function setProducts(array $products)
    {
        $this->products = $products;
        return $this;
    }

    protected function checkNum($num)
    {
        return ($num != 0 || isset($num)) ? $num . "" : "0";
    }

    protected function getMessage($product)
    {
        $message = $this->document->createElement('Message');
        $message->appendChild($this->document->createElement("MessageID", "{$product->getAmzSku()}"));
        $message->appendChild($this->document->createElement("OperationType", $this->operation));
        return $message;
    }

    public function saveXML()
    {
        $filename = static::$name . '_'. date('YmdHis') . '.xml';
        $xml = $this->document->saveXML();

        $data = [
            'feed_type' => $this->feedType,
            'xml' => $filename,
        ];

        file_put_contents(TMP . $filename, $xml);
        $entity = $this->Feeds->newEntity($data);
        $this->Feeds->save($entity);
    }

    abstract protected function getElement(\Amz\Interfaces\ProductAmzInterface $product);
    
    public function addMessage($product)
    {
        $fragment = $this->document->createDocumentFragment();
        $element = $this->getElement($product);
        $fragment->appendXML(explode("\n", $element->asXML())[1]);
        $message = $this->getMessage($product);
        $message->appendChild($fragment);
        $this->root->appendChild($message);
    }

}
