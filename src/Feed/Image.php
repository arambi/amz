<?php

namespace Amz\Feed;

use Amz\Feed\Feed;

class Image extends Feed
{
    public static $name = 'ProductImage';

    protected $feedType = 'POST_PRODUCT_IMAGE_DATA';

    protected $url;

    protected $imageType;

    protected function getElement(\Amz\Interfaces\ProductAmzInterface $product)
    {
    }

    protected function getElementImage(\Amz\Interfaces\ProductAmzInterface $product, $image_type, $image_url)
    {
        $element = new \SimpleXMLElement("<ProductImage/>");
        $element->addChild("SKU", $product->getAmzSku());
        $element->addChild("ImageType", $image_type);
        $element->addChild("ImageLocation", $image_url);
        return $element;
    }

    public function addMessage($product)
    {
        foreach ($product->getAmzImages() as $image_type => $image_url) {
            $fragment = $this->document->createDocumentFragment();
            $element = $this->getElementImage($product, $image_type, $image_url);
            $fragment->appendXML(explode("\n", $element->asXML())[1]);
            $message = $this->getMessage($product);
            $message->appendChild($fragment);
            $this->root->appendChild($message);
        }

    }
}
