<?php

namespace Amz\Feed;

use Amz\Feed\Feed;

class Product extends Feed
{
    public static $name = 'Product';

    protected $feedType = 'POST_PRODUCT_DATA';
 
    protected function getElement(\Amz\Interfaces\ProductAmzInterface $product)
    {
        $root = new \SimpleXMLElement("<Product/>");
        $root->addChild("SKU", $product->getAmzSku());
        $stdID = $root->addChild("StandardProductID");
        $stdID->addChild("Type", "EAN");
        $stdID->addChild('Value', $product->getAmzEAN());
        $root->addChild('LaunchDate', date('c', strtotime('-1 day')));
        $root->addChild('ReleaseDate', date('c', strtotime('-1 day')));
        $cond = $root->addChild("Condition");
        $cond->addChild("ConditionType", 'New');
        $root->addChild('ItemPackageQuantity', 1);
        $root->addChild('NumberOfItems', 1);
        $desc = $root->addChild("DescriptionData");
        $desc->addChild("Title", $product->getAmzTitle());
        $desc->addChild("Brand", $product->getAmzBrand());
        $desc->addChild("Description", $product->getAmzDescription());
        $product->getAmzBulletPoints($desc);
        $weight = $desc->addChild("PackageWeight", $this->checkNum($product->store_weight));
        $weight->addAttribute("unitOfMeasure", "GR");
        $shipW = $desc->addChild("ShippingWeight",  $this->checkNum($product->store_weight));
        $shipW->addAttribute("unitOfMeasure", "GR");
        $desc->addChild("Manufacturer", $product->getAmzBrand());
        $desc->addChild("MfrPartNumber", $product->getAmzMfrPartNumber());
        $desc->addChild("CountryOfOrigin", 'ES');
        $pd = $root->addChild("ProductData");
        $pd
            ->addChild("AutoAccessory")
            ->addChild("ProductType")
            ->addChild("VehicleBrakePad");
        return $root;
    }

}
