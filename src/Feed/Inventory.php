<?php

namespace Amz\Feed;

use Amz\Feed\Feed;

class Inventory extends Feed
{
    public static $name = 'Inventory';

    protected $feedType = 'POST_INVENTORY_AVAILABILITY_DATA';
 
    protected function getElement(\Amz\Interfaces\ProductAmzInterface $product)
    {
        $element = new \SimpleXMLElement("<Inventory/>");
        $element->addChild("SKU", $product->getAmzSku());
        $element->addChild("Quantity", $this->checkNum($product->getAmzQuantity()));
        return $element;
    }
}
