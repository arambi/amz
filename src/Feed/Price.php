<?php

namespace Amz\Feed;

use Amz\Feed\Feed;

class Price extends Feed
{
    public static $name = 'Price';

    protected $feedType = 'POST_PRODUCT_PRICING_DATA';
 
    protected function getElement(\Amz\Interfaces\ProductAmzInterface $product)
    {
        $element = new \SimpleXMLElement("<Price/>");
        $element->addChild("SKU", $product->getAmzSku());
        $strPrice = $element->addChild("StandardPrice", round($product->getAmzPrice(), 2));
        $strPrice->addAttribute("currency", $this->config->currency);
        return $element;
    }

}
